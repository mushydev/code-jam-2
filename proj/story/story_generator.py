import random
from json import load

from proj.steam import steamcheck


def load_story():
    """
    Loads stories - stories.json
    """

    with open('\proj\story\stories.json') as json_file:
        config = load(json_file)
        return config


def random_order(seed: int):
    """
    Generates a random order of integers
    Based on a seed
    """

    random.seed(seed)
    snippets_order = random.sample(range(1, 6), 4)
    return snippets_order


def status(steam_id):
    """
    Generates a status response
    """

    new_id = steamcheck.get_valid_steam64(steam_id)
    if new_id:
        return "The given steam id does exist"
    return "The given steam id does not exist"


def generate_story(steam_id):
    """
    Generates story based on steam id
    """

    new_id = steamcheck.get_valid_steam64(steam_id)

    if new_id:
        order = random_order(new_id)
        stories = load_story()
        beginning = stories['beginning'][f'{order[0]}']
        part_1 = stories['part1'][f'{order[1]}']
        part_2 = stories['part2'][f'{order[2]}']
        ending = stories['ending'][f'{order[3]}']
        return beginning + "\n" + part_1 + "\n" + part_2 + "\n" + ending
    return ""
