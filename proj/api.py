from flask import Flask
from flask_restful import Api, Resource, fields, marshal, reqparse

from proj.story import story_generator
from proj.story.story_generator import status


app = Flask(__name__)
api = Api(app)


myth_fields = {
    'steamid64': fields.String,
    'story': fields.String,
    'status': fields.String
}


class MythAPI(Resource):
    """
    API serves random stories.
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'steamid64',
            type=str,
            required=True,
            help='No Steam/ID username was provided.',
            location='json'
        )
        super(MythAPI, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        new_myth = {
            'status': status(args['steamid64']),
            'steamid64': args['steamid64'],
            'story': story_generator.generate_story(args['steamid64'])

        }
        return {'steam_myth': marshal(new_myth, myth_fields)}, 201


api.add_resource(MythAPI, '/api/v1/myths/', endpoint='myths')

if __name__ == '__main__':
    app.run()
