from json import load


def load_config():
    """
    Loads JSON file - config.json
    """
    with open('proj/config.json') as json_file:
        config = load(json_file)
        return config


STEAM_API_KEY = load_config()['steam_api_key']
