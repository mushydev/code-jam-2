# Code Jam 2: Mythology API

It's necessary to have an environment variable called STEAM_API_KEY with a valid steam api key.


This api builds a random mythological story based on a seed, in this case the seed is your steam64id.

You can either post your steam64id or your custom one, since the api checks its format and get's the steam64id if the
custom id was given.

If hosted locally this would be a way of calling the api:


import requests

data = {
    "steamid64": "suriste234s"
}
r = requests.post(url="http://127.0.0.1:5000/api/v1/myths/", json=data)





>>>
    <Response [201]>
{
    "steam_myth": {
        "steamid64": "suriste234s",
        "story": "",
        "status": "The given steam id was not correct"
    }
}
