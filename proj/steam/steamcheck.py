import json
import os

import requests


def is_valid_steam64id(id_steam):
    """
    Checks whether the input is a Steam64ID key.
    """

    return id_steam.isnumeric() and len(id_steam) == 17


def get_valid_steam64(id_steam: str):
    if is_valid_steam64id(id_steam):
        return int(id_steam)
    else:
        # Create a connection to Steam API
        key = os.environ['STEAM_API_KEY']
        url = (
            "https://api.steampowered.com/ISteamUser/ResolveVanityURL/"
            f"v1/?key={key}&vanityurl={id_steam}"
        )

        response = requests.get(url)
        response_json = response.text.replace("'", "\"")
        json_dict = json.loads(response_json)  # Convert raw contents to dict
        if json_dict['response']['success'] == 1:
            return int(json_dict['response']['steamid'])
        return False
